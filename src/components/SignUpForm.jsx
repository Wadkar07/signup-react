import React, { Component } from 'react'
import './SignUpForm.css'
import validatorFunction from './validatorFunction';

export class SignUpForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            age: '',
            email: '',
            password1: '',
            password2: '',
            errors: {},
            onSubmitError: '',
            onSubmitSuccess: ''
        }
    }

    handleChange = (event) => {
        const { name, value } = event.target;

        this.setState({
            [name]: value
        })

        let error = (name === 'password2') ? validatorFunction(name, value, this.state.password1) : validatorFunction(name, value);

        this.setState((previousState) => {
            const errorLog = {
                ...this.state.errors,
                [name]: error
            }
            return {
                ...previousState,
                errors: errorLog
            }
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { errors } = this.state;
        let errorCount = Object.entries(errors).reduce((error, [errorField, errorMessage]) => (errorMessage ? (error[errorField] = errorMessage, error) : error), {});
        if (Object.keys(errorCount).length > 0) {
            this.setState({
                onSubmitError: "please resolve error first",
                onSubmitSuccess: ""
            })
        }
        else {
            this.setState({
                onSubmitSuccess: "Singup Success",
                onSubmitError: ""
            })
        }
    }

    render() {
        const {
            onSubmitError,
            onSubmitSuccess
        } = this.state;

        const {
            firstName,
            lastName,
            age,
            email,
            password1,
            password2,
        } = this.state.errors

        return (
            <div className='m-3 mt-5 d-flex justify-content-center '>
                <form className='shadow-lg mb-5 bg-body-tertiary rounded col-xl-5' onSubmit={this.handleSubmit}>

                    <div className="text-center">
                        <legend className="p-3">Sign Up</legend>
                    </div>

                    <div className="mb-4 form-group">
                        <label htmlFor="fname">First Name</label>
                        <input type="text" id="fname" name="firstName" className="form-control pr-2" placeholder="First name" onChange={this.handleChange} />
                        <small className='position-absolute text-danger'>{firstName}</small>
                    </div>

                    <div className="mb-4 form-group">
                        <label htmlFor="lname">Last Name</label>
                        <input type="text" id="lname" name="lastName" className="form-control pr-2" placeholder="Last name" onChange={this.handleChange} />
                        <small className='position-absolute text-danger'>{lastName}</small>
                    </div>

                    <div className="mb-4 form-group">
                        <label htmlFor="age">Age</label>
                        <input type="text" name="age" className="form-control" id="age" placeholder="age" required onChange={this.handleChange} />
                        <small className='position-absolute text-danger'>{age}</small>
                    </div>

                    <div className="genders my-2 py-2">
                        <label className='pr-2'>Gender</label>

                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="gender" id="female" value="female" />
                            <label className="form-check-label" htmlFor="female">Female</label>
                            <small className="position-absolute name-error"></small>
                        </div>

                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="gender" id="male" value="male" />
                            <label className="form-check-label" htmlFor="male">Male</label>
                        </div>

                        <div className="form-check form-check-inline">
                            <input className="form-check-input" type="radio" name="gender" id="other" value="other" />
                            <label className="form-check-label" htmlFor="other">Other</label>
                        </div>

                    </div>

                    <div className="mb-3 form-group">
                        <label htmlFor="Select1">Role <i className="fa-solid fa-user-tie"></i></label>
                        <select name="role" className="form-control" id="Select1" required onChange={this.handleChange}>
                            <option>Developer</option>
                            <option>Senior Developer</option>
                            <option>Lead Engineer</option>
                            <option>CTO</option>
                        </select>
                    </div>

                    <div className="mb-4 form-group">
                        <label htmlFor="Email1">Email address <i className="fa-regular fa-envelope"></i></label>
                        <input type="email" name="email" className="form-control" id="Email1" aria-describedby="emailHelp" placeholder="Enter email" required onChange={this.handleChange} />
                        <small className='position-absolute text-danger'>{email}</small>
                    </div>

                    <div className="mb-4 form-group">
                        <label htmlFor="Password1">Password <i className="fa-solid fa-lock"></i></label>
                        <input type="password" name="password1" className="form-control" id="Password1" placeholder="Password" required onChange={this.handleChange} />
                        <small className='position-absolute text-danger'>{password1}</small>
                    </div>

                    <div className="mb-4 form-group">
                        <label htmlFor="Password2">Repeat Password <i className="fa-solid fa-lock"></i></label>
                        <input type="password" name="password2" className="form-control" id="Password2" placeholder="Password" required onChange={this.handleChange} />
                        <small className='position-absolute text-danger'>{password2}</small>
                    </div>

                    <div className="form-group row d-flex justify-content-center">
                        <div className="form-check">
                            <input name="check" className="form-check-input" type="checkbox" id="gridCheck1" required />
                            <label className="form-check-label" htmlFor="gridCheck1">
                                Agree to ToS <i className="fa-solid fa-file-contract"></i>
                            </label>
                        </div>
                    </div>

                    <div className="mb-4 d-flex flex-column align-items-center form-group">
                        <button type="submit" className="mt-5 col-xl-5 btn btn-success">Submit</button>
                        <small className='text-success position-absolute'>{onSubmitSuccess}</small>
                        <small className='text-danger position-absolute'>{onSubmitError}</small>
                    </div>

                </form>
            </div>
        )
    }
}

export default SignUpForm