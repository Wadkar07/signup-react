import validator from 'validator'

export default function validatorFunction(name,value,password1) {
    if(name === 'firstName'&& !validator.isAlpha(value)){
        return 'Enter first name correctly';
    }
    else if(name === 'lastName'&& !validator.isAlpha(value)){
        return 'Enter last name correctly';
    }
    else if(name === 'age'&& (!validator.isInt(value) || value <=17 || value >= 65)){
        return 'Enter age between 18-65';
    }
    else if(name === 'email'&& !validator.isEmail(value)){
        return 'Enter valid mail';
    }
    else if(name === 'password1'&& !validator.isStrongPassword(value)){
        return 'Password is not strong enough';
    }
    else if(name === 'password2'&& (!validator.isStrongPassword(value) || password1!==value)){
        return `Password is not matching`;
    }
    else{
        return '';
    }
}
